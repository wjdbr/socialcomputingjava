import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.transaction.xa.Xid;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

//电影爬数据类
enum PersonType{
	enuDirector,
	enuWriter,
	enuStar
};

public class MovieReg {
    public static String mMovieItemUrl = "http://movie.douban.com/subject/";

	//正则表达式
	public static String mSearchReg = "<a.*?href=\"(http://movie.douban.com/subject.*?)\"";
	public static String mItemReg = "property=\"v:itemreviewed\">(.*?)</span>|property=\"v:average\">(.*?)</strong>|rel=\"v:directedBy\">(.*?)</a>|<span class=\"attrs\">(.*)</span>|property=\"v:genre\">(.*?)</span>|rel=\"v:starring\">(.*?)</a>|property=\"v:votes\">(.*?)</span>|class=\"all hidden\".*?>(.*?)</span>";
	
	public static String mNameReg = "property=\"v:itemreviewed\">(.*?)</span>";
	public static String mRateReg = "property=\"v:average\">(.*?)</strong>";
	public static String mDirectorReg = "rel=\"v:directedBy\">(.*?)</a>";
	public static String mWriterReg = "<a href=\"/celebrity/.*?/\">(.*?)</a>";
	public static String mGenreReg = "property=\"v:genre\">(.*?)</span>";
	public static String mActorReg = "rel=\"v:starring\">(.*?)</a>";
	public static String mPeopleReg = "property=\"v:votes\">(.*?)</span>";
	public static String mDescriptionReg = "property=\"v:summary\".*?>(.*?)</span>";
	public static String mYearReg = "class=\"year\">(.*?)</span>";
	
	public static String mShortReviewReg = "<a href=\"http://movie.douban.com/subject/.*?/comments\">全部 (.*?) 条</a>";
	public static String mSeenNumReg = "<a href=\"http://movie.douban.com/subject/.*?/collections\">(.*?)人看过</a>";
	public static String mWantNumReg = "<a href=\"http://movie.douban.com/subject/.*?/wishes\">(.*?)人想看</a>";
	public static String mReviewReg = "<a href=\"http://movie.douban.com/subject/.*?/reviews\">全部(.*?)</a>";
	public static String mRecMovieReg = "<div class=\"recommendations-bd\"></div>";
	public static String mAreaReg = "class=\"pl\">制片国家/地区:</span>\"(.*?)\"<br>";
	
	public static String mBaiduSearchPrefix = "http://www.baidu.com/s?wd=";
	public static String mBaiduResult = "百度为您找到相关结果约(.*?)个";
	
	public static String mDirIDReg = "<a href=\"/celebrity/(.*?)/\" rel=\"v:directedBy\">";
	public static String mStarIDReg = "<a href=\"/celebrity/(.*?)/\" rel=\"v:starring\">";
	public static String mWriterIDReg = "<a href=\"/celebrity/(.*?)/\">";
	
	//一些辅助变量
	static int m_count = 0;
	static int m_itemCount = 0;
	static ArrayList<String> m_listNames;
	
	//得到正则匹配结果集合
	static HashSet<String> getResultList(String strRegex,String strResult) {
		HashSet<String> listResult = new HashSet<String>();
		Pattern pat = Pattern.compile(strRegex,Pattern.DOTALL | Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
		Matcher mat = pat.matcher(strResult);
		boolean isFind = mat.find();
		while(isFind) {
			for(int i = 1;i <= mat.groupCount();i++) {
				if(mat.group(i) != null) {
					listResult.add(mat.group(i).trim().replace("<br />", "").replace("\n", ""));
				}
			}
			isFind = mat.find();
		}

		return listResult;
	}
	//得到与此同时匹配结果string
	static String getResult(String strRegex,String strResult) {
		String result = "";
		Pattern pat = Pattern.compile(strRegex,Pattern.DOTALL | Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
		Matcher mat = pat.matcher(strResult);
		boolean isFind = mat.find();
		while(isFind) {
			for(int i = 1;i <= mat.groupCount();i++) {
				if(mat.group(i) != null) {
					String res = mat.group(i).trim().replace("<br />", "").replace("\n", "");
					result += result == "" ? res : "," + res;
				}
			}
			isFind = mat.find();
		}
		return result;
	}
	
	static String getWriteResult(String strRegex,String strResult) {
		String result = "";
		int count = 0;
		Pattern pat = Pattern.compile(strRegex,Pattern.DOTALL | Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
		Matcher mat = pat.matcher(strResult);
		boolean isFind = mat.find();
		while(isFind) {
			if (count > 2) {
				break;
			}
			for(int i = 1;i <= mat.groupCount();i++) {
				if(mat.group(i) != null) {
					String res = mat.group(i).trim().replace("<br />", "").replace("\n", "");
					if (res.contains("<")) {
						continue;
					}
					result += result == "" ? res : "," + res;
					count++;
					if (count > 2) {
						break;
					}
				}
			}
			isFind = mat.find();
		}
		return result;
	}
	
	
	static String getWriterResult(String strRegex,String strResult) {
		String result = "";
		Pattern pat = Pattern.compile(strRegex,Pattern.DOTALL | Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
		Matcher mat = pat.matcher(strResult);
		boolean isFind = mat.find();
		int count = 0;
		while(isFind) {
			if (count > 2) {
				break;
			}
			for(int i = 1;i <= mat.groupCount();i++) {
				if(mat.group(i) != null) {
					String res = mat.group(i).trim().replace("<br />", "").replace("\n", "");
					if (res.contains("<") && result != "") {
						continue;
					}
					result += result == "" ? res : "," + res;
					count++;
					if (count > 2) {
						break;
					}
				}
			}
			isFind = mat.find();
		}
		String[] results = result.split("/");
		return results[results.length - 1];
}
	
	static String getStarResult(String strResult) {
		String[] strs = strResult.split("/");
		String result = strs[strs.length - 1];
		String res = "";
		String[] ids = result.split(",");
		for (int i = 0; i < 3; i++) {
			res += res == "" ? ids[i] : "," + ids[i];
		}
		return res;
	}
	
	static String getStaringResult(String strRegex,String strResult) {
		String result = "";
		int count = 0;
		Pattern pat = Pattern.compile(strRegex,Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		Matcher mat = pat.matcher(strResult);
		boolean isFind = mat.find();
		while(isFind) {
			if (count > 2) {
				break;
			}
			for(int i = 1;i <= mat.groupCount();i++) {
				if(mat.group(i) != null) {
					String res = mat.group(i).trim().replace("<br />", "").replace("\n", "");
					if (res.contains("<")) {
						continue;
					}
					result += result == "" ? res : "," + res;
					count++;
					if (count > 2) {
						break;
					}
				}
			}
			isFind = mat.find();
		}
		return result;
	}
	
	//通过电影名字得到搜索结果集合
    static HashSet<String> getSearchResult(String strKey) {
		String strReg = MovieReg.mSearchReg;
		String result = WebRequest.sendPost("http://movie.douban.com/subject_search", "search_text=" + strKey + "&cat=1002");
		HashSet<String> listResult = getResultList(strReg, result);
		return listResult;
	}
    
    //在一个电影页面抓取，并根据相应条件写入xml
    static void crawlInPage(String strUrl,String fileToMake) throws MalformedURLException {
		String pageResult = WebRequest.getPageResult(strUrl);
		String strRate = getResult(mRateReg, pageResult);
//		if (strRate == null || strRate.trim().length() == 0) {
//			return;
//		}
		String strName = getResult(mNameReg, pageResult);
//		if (!m_listNames.get(m_itemCount).contains(strName) && !strName.contains(m_listNames.get(m_itemCount))) {
//			return;
//		}
		String strYear = getResult(mYearReg, pageResult).replace("(", "").replace(")", "");
//		if (strYear.compareTo("2014") < 0) {
//			return;
//		}
		String strActor = getStaringResult(mActorReg, pageResult);
		String strdesc = getResult(mDirectorReg, pageResult);
		String strPeople = getResult(mPeopleReg, pageResult);
		String strDirector = getResult(mDirectorReg, pageResult);
		String strGenre = getResult(mGenreReg, pageResult);
		
		String strShortReview = getResult(mShortReviewReg, pageResult);
		String strSeenNum = getResult(mSeenNumReg, pageResult);
		String strWantNum = getResult(mWantNumReg, pageResult);
		String strReviewReg = getResult(mReviewReg, pageResult);
		String strRec = getResult(mRecMovieReg, pageResult);
		String strArea = getResult(mAreaReg, pageResult);
		String strWriter = getWriteResult(mWriterReg, pageResult);
		String strDirID = getResult(mDirIDReg, pageResult);
		String strStarID = getStarResult(getResult(mStarIDReg, pageResult));
		String strWriterID = getWriterResult(mWriterIDReg, pageResult);
		
		Map<String, String> movieItem = new LinkedHashMap<String, String>();
		movieItem.put("ID", strUrl.replace("http://movie.douban.com/subject/", ""));
		movieItem.put("name", strName);
		movieItem.put("year", strYear);
		movieItem.put("area", strArea);
		movieItem.put("director", strDirector);
		
	    String[] dirs = strDirector.split(",");
	    String dirIndex = "";
	    for (String item : dirs) {
			dirIndex += dirIndex.length() == 0 ? MovieReg.getBaiduIndex(item) : "," + MovieReg.getBaiduIndex(item);
		}
	    movieItem.put("dirIndex", dirIndex);
	    
		movieItem.put("writer", strWriter);
		
		String[] writers = strWriter.split(",");
	    String writerIndex = "";
	    for (String item : writers) {
			writerIndex += writerIndex.length() == 0 ? MovieReg.getBaiduIndex(item) : "," + MovieReg.getBaiduIndex(item);
		}
	    movieItem.put("writerIndex", writerIndex);
	    
		movieItem.put("starring", strActor);
		String[] stars = strActor.split(",");
	    String starIndex = "";
	    for (String item : stars) {
			starIndex += starIndex.length() == 0 ? MovieReg.getBaiduIndex(item) : "," + MovieReg.getBaiduIndex(item);
		}
	    movieItem.put("starIndex", starIndex);
		
		movieItem.put("genre", strGenre);
		//movieItem.put("description", strdesc);
		movieItem.put("rateNum", strPeople);
		movieItem.put("seenNum", strSeenNum);
		movieItem.put("wantNum", strWantNum);
		movieItem.put("shortReviewNum", strShortReview);
		movieItem.put("reviewNum",strReviewReg);
		movieItem.put("recMovie", strRec);
		movieItem.put("rate", strRate);
		//movieItem.put("dirID", strDirID);
		//movieItem.put("starID",strStarID);
		movieItem.put("directorMovieRate", getCeleMoviesRate(strDirID, PersonType.enuDirector));
		movieItem.put("starMovieRate", getCeleMoviesRate(strStarID, PersonType.enuStar).toString());
		movieItem.put("writerMovieRate", getCeleMoviesRate(strWriterID, PersonType.enuWriter).toString());
		movieItem.put("rec10Rate", getRec10(strUrl).toString());
		//movieItem.put("writerID", strWriterID);
		Map<String, Float> genreRates = getGenreRates("./Data/genreRates.xml");
		String[] strGenres = strGenre.split(",");
		Float sum = (float)0;
		for (String item : strGenres) {
			if (genreRates.containsKey(item)) {
				sum += genreRates.get(item);
			}
			else {
				sum += 5;
			}
		}
		sum /= strGenres.length;
		movieItem.put("genreRate", sum.toString());
		
		XmlOpt xo = new XmlOpt();
		xo.createXml(fileToMake, movieItem);
		//System.out.println(String.format("%3d", m_count) + "  done");
		m_count++;
	}
    
    //读电影名字
    static ArrayList<String> readFile(String fileName) throws IOException {
    	ArrayList<String> listNames = new ArrayList<String>();
    	File f = new File(fileName);
    	BufferedReader br = new BufferedReader(new FileReader(f));
    	String line = "";
    	while((line = br.readLine()) != null) {
    		listNames.add(line.trim());
    	}
    	br.close();
    	return listNames;
    }
    
    //从电影名字文件中得到xml文件
    public static void getMovieXml(String fileName,String xmlFile) throws IOException {
		m_listNames = readFile(fileName);
		for(String item : m_listNames) {
			HashSet<String> searchResult = getSearchResult(item);
			for(String result : searchResult) {
				crawlInPage(result,xmlFile);
			}
			System.out.println(m_itemCount);
			m_itemCount++;
		}
	}
	
    //删除无用电影信息
	public static void deleteInvalid() {
		ArrayList<Integer> deleteList = new ArrayList<Integer>();
		deleteList.add(0);
		deleteList.add(2);
		deleteList.add(4);
		deleteList.add(8);
		deleteList.add(14);
		deleteList.add(16);
		deleteList.add(19);
		deleteList.add(31);

		XmlOpt xo = new XmlOpt();
		xo.deleteNodeByID("./Data/MoviesToPre.xml", deleteList);
		System.out.println("done");
	}
	
	//得到一个item的百度搜索结果数量
	static String getBaiduIndex(String item) {
		String pageResult = WebRequest.getPageResult(mBaiduSearchPrefix + item);
		String result = getResult(mBaiduResult, pageResult).replace(",", "");
		return result;
	}
	
	//得到一个导演的最近10个电影平均分,不满10个则取所有电影平均分
	static Float getMoviesRate(String dirID,PersonType type) {
		String dirUrl = genCelebrityString(type, dirID);
		String pageResult = WebRequest.getPageResult(dirUrl);
		String strReg = "<span class=\"rating_nums\">(.*?)</span>";
		String strResult = getResult(strReg, pageResult);
		int count = 0;
		Float sum = (float) 0;
		String[] results = strResult.split(",");
		for (String item : results) {
			if (item.trim().length() == 0) {
				continue;
			}
			Float s =Float.parseFloat(item);
			sum += s;
			count++;
			if (count >= results.length || count > 9) {
				break;
			}
		}
		Float avg = sum / count;
		return avg;
	}
	
	//得到演过的电影平均分，这几个id用逗号隔开
	static String getCeleMoviesRate(String dirIDs,PersonType type) {
		Float avgs = (float) 0;
		String[] dirIDss = dirIDs.split(",");
		int count = 0;
		for (String item : dirIDss) {
			if (item.trim().length() == 0) {
				continue;
			}
			Float avg = getMoviesRate(item,type);
			avgs += avg;
			count++;
		}
		avgs /= count;
		return avgs.toString();
	}
	
	static String genCelebrityString(PersonType type,String item) {
		String str = "";
		switch (type) {
		case enuDirector:       //导演
			str = String.format("http://movie.douban.com/celebrity/%s/movies?sortby=time&format=text&role=D", item);
			break;
		case enuWriter:       //编剧
			str = String.format("http://movie.douban.com/celebrity/%s/movies?sortby=time&format=text&role=W", item);
			break;
		case enuStar:       //演员
			str = String.format("http://movie.douban.com/celebrity/%s/movies?sortby=time&format=text&role=A", item);
			break;
			
		default:
			break;
		}
		return str;
	}
	
	static void setGenreRates(String fileName,String fileToMake) {
		XmlOpt xo = new XmlOpt();
		Map<String, Integer> genreCount = new HashMap<String, Integer>();
	    Map<String, Float> genreRates = new HashMap<String, Float>();
	    List<String> genreList = xo.searchNode(fileName, "/movies/movie/genre");
	    List<String> rateList = xo.searchNode(fileName, "/movies/movie/rate");
	    for (int i = 0; i < genreList.size(); i++) {
			String genre = genreList.get(i);
			String[] genres = genre.split(",");
			for (String item : genres) {
				if (!genreCount.containsKey(item)) {
					genreCount.put(item, 1);
					genreRates.put(item, Float.parseFloat(rateList.get(i)));
				}
				else {
					genreCount.put(item, genreCount.get(item)+1);
					genreRates.put(item, Float.parseFloat(rateList.get(i)) + genreRates.get(item));
				}
			}
		}
	    for (String item : genreCount.keySet()) {
			genreRates.put(item, genreRates.get(item) / genreCount.get(item));
		    Map<String, String> genreMap = new HashMap<String, String>();
			genreMap.put("genre", item);
			genreMap.put("rate", genreRates.get(item).toString());
		    xo.createXml(fileToMake, genreMap);
		}
	}
	
	static Map<String, Float> getGenreRates(String fileName) {
		XmlOpt xo = new XmlOpt();
		List<String> genreList = xo.searchNode(fileName, "/movies/movie/genre");
		List<String> rateList = xo.searchNode(fileName, "/movies/movie/rate");
		Map<String, Float> map = new HashMap<String, Float>();
		for(int i = 0;i < genreList.size();i++) {
			map.put(genreList.get(i), Float.parseFloat(rateList.get(i)));
		}
		return map;
	}
	
	static void getTrainingList() {
		String pageBaseUrl = "http://www.douban.com/doulist/1295618/?start=";
		String pageSuffix = "&sort=seq";
		int delta = 25;
		for (int i = 0; i < 22; i++) {
			String pageUrl = pageBaseUrl + delta * i + pageSuffix;
			String pageContent = WebRequest.getPageResult(pageUrl);
			String regex = "<a href=\"http://movie.douban.com/subject/(.*?)/\" target=\"_blank\">";
			String result = getResult(regex, pageContent);
			HashSet<String> set = getResultList(regex, pageContent);
			
			String[] results = result.split(",");
			for (String item : set) {
				Map<String, String> maps = new HashMap<String, String>();
				maps.put("ID", item);
				XmlOpt xo = new XmlOpt();
				xo.createXml("./Data/trainingListID.xml", maps);
			}
			
		}
		System.out.println("done");
	}
	
	static Float getRec10(String pageUrl) {
		String pageContent = WebRequest.getPageResult(pageUrl);
		String regex = "<a href=\"http://movie.douban.com/subject/(.*?)/?from=subject-page";
		String result = getResult(regex, pageContent);
		String[] results = result.split("subject/");
		result = results[results.length - 1].replace("/?", "");
		String[] finalResult = result.split(",");
		Float sum = (float)0;
		int count = 0;
		for (String item : finalResult) {
			if (item.trim().length() == 0) {
				continue;
			}
			String pUrl = mMovieItemUrl + item;
			Float rate = getMovieRate(pUrl);
			sum += rate;
			count++;
		}
		sum /= count;
		return sum;
	}
	
	static Float getMovieRate(String pageUrl) {
		String pageContent = WebRequest.getPageResult(pageUrl);
		String result = getResult(mRateReg, pageContent);
		return result.trim().length() == 0 ? (float)5 : Float.parseFloat(result);
	}
	
	
};
