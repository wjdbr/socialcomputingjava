import java.awt.List;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

//电影xml操作类

public class XmlOpt {
	DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
    Element theNode=null, theElem=null, root=null;
    
    //创建xml，追加一个条目
	public boolean createXml(String fileName,Map<String, String> movieItem) {
	    try {
	    	factory.setIgnoringElementContentWhitespace(true);
	        DocumentBuilder db=factory.newDocumentBuilder();
	        Document xmldoc=db.parse(new File(fileName));
	        root=xmldoc.getDocumentElement();
	        theNode=xmldoc.createElement("movie");

        	Set<String> key = movieItem.keySet();
            for (Iterator<String> it = key.iterator(); it.hasNext();) {
                String s = (String) it.next();
                String value = movieItem.get(s);
                theElem = xmldoc.createElement(s);
    	        theElem.setTextContent(value);
    	        theNode.appendChild(theElem);
            }
	        root.appendChild(theNode);
            saveXml(fileName, xmldoc);
            return true;
	    } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	    return false;
    }
	
	//输出xml
	 public void output(Node node) {//将node的XML字符串输出到控制台
		 TransformerFactory transFactory=TransformerFactory.newInstance();
		 try {
			 Transformer transformer = transFactory.newTransformer();
			 transformer.setOutputProperty("encoding", "gb2312");
			 transformer.setOutputProperty("indent", "yes");
	
			 DOMSource source=new DOMSource();
			 source.setNode(node);
			 StreamResult result=new StreamResult();
			 result.setOutputStream(System.out);
	        
			 transformer.transform(source, result);
		 } catch (TransformerConfigurationException e) {
	        e.printStackTrace();
		 } catch (TransformerException e) {
	        e.printStackTrace();
		 }   
	}
	 
	//查找节点，并返回第一个符合条件节点
	public Node selectSingleNode(String express, Object source) {
        Node result=null;
        XPathFactory xpathFactory=XPathFactory.newInstance();
        XPath xpath=xpathFactory.newXPath();
        try {
            result=(Node) xpath.evaluate(express, source, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        
	    return result;
	}
	    
	//查找节点，返回符合条件的节点集。
    public NodeList selectNodes(String express, Object source) {
        NodeList result=null;
        XPathFactory xpathFactory=XPathFactory.newInstance();
        XPath xpath=xpathFactory.newXPath();
        try {
            result=(NodeList) xpath.evaluate(express, source, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        
        return result;
    }
	    
    //将Document输出到文件
    public void saveXml(String fileName, Document doc) {
        TransformerFactory transFactory=TransformerFactory.newInstance();
        try {
            Transformer transformer = transFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            DOMSource source=new DOMSource();
            source.setNode(doc);
            StreamResult result=new StreamResult();
            result.setOutputStream(new FileOutputStream(fileName));
            
            transformer.transform(source, result);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }   
    }
    
    //通过电影ID删除电影
    public void deleteNodeByID(String fileName,ArrayList<Integer> deleteList) {
    	try {
	    	factory.setIgnoringElementContentWhitespace(true);
	        DocumentBuilder db=factory.newDocumentBuilder();
	        Document xmldoc=db.parse(new File(fileName));
	        root=xmldoc.getDocumentElement();
	        theNode=xmldoc.createElement("movie");

	        for(Integer item : deleteList) {
	        	String id = String.format("/movies/movie[ID=%d]", item);
	        	theNode = (Element) selectSingleNode(id, root);
	            theNode.getParentNode().removeChild(theNode);
	        }
	        
            saveXml(fileName, xmldoc);
	    } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
    
    //查找指定结点信息，返回一个list string
    public java.util.List<String> searchNode(String fileName,String nodeName) {
    	try {
	    	factory.setIgnoringElementContentWhitespace(true);
	        DocumentBuilder db=factory.newDocumentBuilder();
	        Document xmldoc=db.parse(new File(fileName));
	        root=xmldoc.getDocumentElement();
            NodeList result = this.selectNodes(nodeName, root);
            java.util.List<String> nodeList = new ArrayList<String>();
            
            for (int i = 0;i < result.getLength();i++) {
            	String content = result.item(i).getTextContent().trim().length() == 0 ? "5" : result.item(i).getTextContent().trim();
				nodeList.add(content);
			}
	        return nodeList;
	    } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    	return null;
    }


}

